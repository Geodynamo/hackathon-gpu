from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

def get_destination(rank, iter):
    """
    Function that returns the process destination in the loop
    Use the exclusive or to get
    """
    binary_repr = lambda N: int(bin(N)[2:], 2)
    return binary_repr(rank) ^ binary_repr(iter)

def manual_all_to_all(vec_to_send):
    """
    vec_to_send: 2d array
    """
    # assert check that number of process is a power of 2
    # assert  that vec can be divided in number of process

    # initialize receving vec
    vec_recv = np.empty_like(vec_to_send)

    ## divide into chunks
    chunk_size = vec_to_send.shape[0] // nprocs
    for i in range(1, nprocs):
        dest = get_destination(rank, i)
        exchanged_slice = slice(dest*chunk_size, (dest+1)*chunk_size)

        if rank < dest:
            # for low rank, first send the data, and higher rank recv it
            comm.Send(vec_to_send[exchanged_slice], dest=dest)
        else:
            comm.Recv(vec_recv[exchanged_slice], source=dest)
        comm.Barrier()

        if rank > dest:
            # in the second time, higher rank sends hist data
            comm.Send(vec_to_send[exchanged_slice], dest=dest)
        else:
            comm.Recv(vec_recv[exchanged_slice], source=dest)

    diagonal_slice = slice(rank*chunk_size, (rank+1)*chunk_size)
    vec_recv[diagonal_slice] = vec_to_send[diagonal_slice]
    return vec_recv

N = 16
vec_to_send = np.arange(0, N) + rank * N
vec_received = manual_all_to_all(vec_to_send)
print('vec_to_send', rank, vec_to_send)
print('vec_received', rank, vec_received)
