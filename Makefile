OMPI_CXX = nvcc
OMPI_CXXFLAGS=$(shell mpicxx -showme:compile |sed 's/-pthread//g')
OMPI_LDFLAGS=$(shell mpicxx -showme:link |sed 's/-pthread//g')

#OPT=" -fsanitize=address -Wtype-limits -Wformat "
# (otherwise the nvcc linker cannot handle the option -Wl,--export-dynamic)
#export OMPI_LIBS= 
#all: check_env a2a_gpu bench
all:  bench

check_env:
	mpirun --version
	ompi_info --parsable --all | grep mpi_built_with_cuda_support:value
	@echo OMPICXXFLAGS $(OMPI_CXXFLAGS)
	@echo OMPI_LDFLAGS $(OMPI_LDFLAGS)

a2a_gpu.o: a2a_gpu.cpp
	mpicxx ${OPT} ${OMPI_CXXFLAGS} -I$(CUDA_HOME)/include -c -O3 a2a_gpu.cpp -o a2a_gpu.o

a2a_gpu: a2a_gpu.o
	mpicxx ${OPT} ${OMPI_LDFLAGS} -O3 -I$(CUDA_HOME)/include a2a_gpu.o -lm -lcudart -lcublas -o $@

bench: a2a_gpu
	sbatch job.gpu
