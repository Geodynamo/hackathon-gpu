
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cuda_runtime_api.h>
double cpu_time_usd;
                                       //
// error checking macro
#define cudaCheckErrors(msg) \
    do { \
        cudaError_t __err = cudaGetLastError(); \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
                msg, cudaGetErrorString(__err), \
                __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(1); \
        } \
    } while (0)


const int lmax = 128;
const int NR = 128;



int get_destination(int rank, int iter){
  int ret = rank ^ iter;
  return ret;
}

long nlm_calc(long lmax, long mmax, long mres)
{
	if (mmax*mres > lmax) mmax = lmax/mres;
	return (mmax+1)*(lmax+1) - ((mmax*mres)*(mmax+1))/2;	// this is wrong if lmax < mmax*mres
}

void non_blocking_exchange(double *d_send, double *d_recv, int rank, int dest, int elem_chunk_size, MPI_Request * request){
    
      if (rank < dest)
        MPI_Isend(d_send+dest*elem_chunk_size, elem_chunk_size, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD, &request[0]);
      else
         MPI_Irecv(d_recv+dest*elem_chunk_size, elem_chunk_size, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD, &request[0]);

      //MPI_Wait(&request, &status); //blocks and waits for destination process to receive data

      if (rank > dest)
          MPI_Isend(d_send+dest*elem_chunk_size, elem_chunk_size, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD, &request[1]);
      else
          MPI_Irecv(d_recv+dest*elem_chunk_size, elem_chunk_size, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD, &request[1]);
  
      // MPI_Wait(&request, &status);
      // MPI_Waitall(2, request, MPI_STATUSES_IGNORE);

}

void blocking_exchange(double *d_send, double *d_recv, int rank, int dest, int elem_chunk_size){
      if (rank < dest)
          MPI_Send(d_send+dest*elem_chunk_size, elem_chunk_size, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD);
      else
          MPI_Recv(d_recv+dest*elem_chunk_size, elem_chunk_size, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      if (rank > dest)
          MPI_Send(d_send+dest*elem_chunk_size, elem_chunk_size, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD);
      else
          MPI_Recv(d_recv+dest*elem_chunk_size, elem_chunk_size, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}


void all_to_all_mano(double *h_send, double* h_recv, int sze){
    clock_t  start, end;
    start=clock();
  
    static double *d_send = 0;
    static double *d_recv = 0;

    int dest;
    int rank;
    int n_mpi;
    MPI_Comm_size(MPI_COMM_WORLD, &n_mpi);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (d_send == 0) {
    	cudaMalloc((void**)&d_send, sze);
    	cudaMalloc((void**)&d_recv, sze);
    }
    cudaMemcpy(d_send, h_send, sze, cudaMemcpyHostToDevice);
    cudaMemcpy(d_recv, h_recv, sze, cudaMemcpyHostToDevice);

    int byte_chunk_size = sze / n_mpi; 
    int elem_chunk_size = byte_chunk_size / sizeof(double);
    
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Request request[2*n_mpi-2];
    double t_elapsed;
    t_elapsed = MPI_Wtime();

    for (int i=1; i < n_mpi; i++){
      dest = get_destination(rank, i);
      non_blocking_exchange(d_send, d_recv, rank, dest, elem_chunk_size, &request[2*(i-1)]);
       //blocking_exchange(d_send, d_recv, rank, dest, elem_chunk_size);
    }
    cudaMemcpy(d_recv+rank*elem_chunk_size, d_send+rank*elem_chunk_size, byte_chunk_size, cudaMemcpyDeviceToDevice);
    MPI_Waitall(2*n_mpi-2, request, MPI_STATUSES_IGNORE);
    MPI_Barrier(MPI_COMM_WORLD);

    t_elapsed = MPI_Wtime() - t_elapsed;
    if (rank==0) printf("Manual Alltoall took %g ms\n", t_elapsed*1000);

    cudaMemcpy(h_recv, d_recv, sze, cudaMemcpyDeviceToHost);
    end = clock();
    cpu_time_usd = ( (double) (end-start)); // This gives clock value on per second basis
    // printf("all_to_all a la mano: %lf \n", cpu_time_usd);
}


void all_to_all(double *h_x, double* h_y, int sze){
    int i_mpi, n_mpi;
    double t_elapsed;
    clock_t start, end;
    start=clock();
    static double *d_x = 0;
    static double *d_y = 0;
    if (d_x == 0) {
    	cudaMalloc((void**)&d_x, sze);
    	cudaMalloc((void**)&d_y, sze);
    }
    cudaMemcpy(d_x, h_x, sze, cudaMemcpyHostToDevice);
    cudaMemcpy(d_y, h_y, sze, cudaMemcpyHostToDevice);
    cudaCheckErrors("cudaMemcpy h -> d failure");
    const int NLM = nlm_calc(lmax,lmax,1);

    MPI_Comm_rank(MPI_COMM_WORLD, &i_mpi);
    MPI_Comm_size(MPI_COMM_WORLD, &n_mpi);
    const int nlm_loc = (NLM+n_mpi-1)/n_mpi;		// round up
    const int nr_loc = NR/n_mpi;
    if (NR % n_mpi) {
	if (i_mpi == 0) printf("ERROR: NR not a multiple of n_mpi\n");
	return;
    }
	
	if (i_mpi==0) printf("field is NR=%d, Lmax=%d ==>  %g MB per process\n", NR, lmax, sze/(1024.*1024.));

    MPI_Barrier(MPI_COMM_WORLD);
	t_elapsed = MPI_Wtime();
	MPI_Alltoall(d_x, 2*nlm_loc*nr_loc, MPI_DOUBLE,
                    d_y, 2*nlm_loc*nr_loc, MPI_DOUBLE, MPI_COMM_WORLD);
	t_elapsed = MPI_Wtime() - t_elapsed;
	if (i_mpi==0) printf("MPI_Alltoall took %g ms\n", t_elapsed*1000);

    cudaMemcpy(h_x, d_x, sze, cudaMemcpyDeviceToHost);
    cudaMemcpy(h_y, d_y, sze, cudaMemcpyDeviceToHost);
    end = clock();
    cpu_time_usd = ( (double) (end-start)); // This gives clock value on per second basis
   // printf("all_to_all mpi: %lf \n", cpu_time_usd);
}

// double* manual_all_to_all(double *h_x, double* h_y, int sze){
// }

int main (int argc, char *argv[])
{
	char* local_rank_env = getenv("SLURM_LOCALID");
	if (local_rank_env) cudaSetDevice( atoi(local_rank_env) );

    MPI_Init(&argc, &argv);
    printf("sizeof(MPI_Request) = %d\n",sizeof(MPI_Request));

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int i_mpi, n_mpi;
    const int NLM = nlm_calc(lmax,lmax,1);
    MPI_Comm_rank(MPI_COMM_WORLD, &i_mpi);
    MPI_Comm_size(MPI_COMM_WORLD, &n_mpi);


	if (local_rank_env) {	// check if the cuda device was correctly set before MPI (SLURM / Jean-Zay)
		int i;
		cudaGetDevice(&i);
		printf("process %d (slurm_localid=%d) uses GPU %d\n", i_mpi, atoi(local_rank_env), i);
	}

    
    const int nlm_loc = (NLM+n_mpi-1)/n_mpi;		// round up
    const int nr_loc = NR/n_mpi;
    const int nb_elem = nlm_loc*nr_loc*n_mpi * 2;
    size_t sze = nb_elem * sizeof(double);
    double* h_x = (double*) malloc(sze);

    int i;
    for (i = 0; i < nlm_loc*nr_loc*n_mpi*2; ++i) {
      h_x[i] = 1.0*i + rank*sze;
    }
    double* h_mpi = (double*) malloc(sze);
    double* h_manual = (double*) malloc(sze);

    for (i=0; i < 10; ++i){
	MPI_Barrier(MPI_COMM_WORLD);
        all_to_all(h_x, h_mpi, sze);    
	MPI_Barrier(MPI_COMM_WORLD);
        all_to_all_mano(h_x, h_manual, sze);
	MPI_Barrier(MPI_COMM_WORLD);
    }

    for (i = 0; i < nlm_loc*nr_loc*n_mpi*2; ++i) {
        if( h_manual[i] != h_mpi[i] ) {
	printf("arrays not equal %d, %f, %f, diff:%f \n", rank, h_manual[i], h_mpi[i], h_manual[i] - h_mpi[i]);
        }
    }
    MPI_Finalize();
}
